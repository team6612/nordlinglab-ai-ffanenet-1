#!/usr/bin/python3
#####
## MIT license applied
##             markliou 2017/12/15
#####

import csv
import tensorflow as tf
import numpy as np
import random
import os

import tools
import FFANEnet

class FFANE4FFANEnet(FFANEnet.FFANE):
    def __init__(self,
                 GeneNo , 
                 fitness,
                 
                 ModelBuilder,
                 Data_handler, 
                 TrainingBasicGenerator, 
                 input_dim, 
                 input_channel_dim, 
                 output_dim, 
                 batch_size,
                 structure_decode,
                 
                 PopSize = 40, 
                 Iteration = 5000,  
                 alpha = 1.0e-3,  
                 beta = 0.5,  
                 gamma = 1,
                 move_constant = np.exp(1),
                 trap_limit = 5,
                 huntered_rate = 0.01 ):
                 
        self.ModelBuilder = ModelBuilder
        self.Data_handler = Data_handler 
        self.TrainingBasicGenerator = TrainingBasicGenerator 
        self.input_dim    = input_dim 
        self.input_channel_dim = input_channel_dim
        self.output_dim   = output_dim
        self.batch_size   = batch_size
        self.structure_decode = structure_decode
        
        self.fitness = fitness # a function
        self.fitness_cnt = 0
        self.trap = 0
        self.PopSize = PopSize
        self.Iteration = Iteration 
        self.GeneNo = GeneNo
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma # absorb coefficient
        self.move_constant = move_constant # inference from PSO
        self.huntered_rate = huntered_rate
        self.trap_limit = trap_limit
        self.Pop = { 'PopSize' : self.PopSize,
                     'GeneNo': self.GeneNo,
                     'CurrentGeneration': 0,
                     'inds' : [ self.gen_ind(GeneNo) for i in range(PopSize)]
                   }

    pass 
    
    def aristogenics(self): # aristogenics using random
        self.Pop['inds'][random.randint(0, (self.PopSize-1))] = self.BestInd.copy() 
    pass
    
    def run(self):
        # initializing
        print("initializing the FFANE process ...")
        # self.BestInd = self.Pop['inds'][0].copy()
        self.update_fitness()
        self.BestInd = self.Pop['inds'][0].copy() # random assign for initializing
        
        # evolution
        print("Start the FFANE process ...")
        for c_iter in range(1,self.Iteration + 1):
            self.update_alpha(c_iter)
            self.update_gamma(c_iter)
            self.update_position_with_NaturalEnemies(c_iter)
            self.update_fitness()
            CBestInd = self.find_the_best(self.Pop)
            if self.BestInd['fitness'] > CBestInd['fitness'] :
                self.BestInd = CBestInd.copy()
            pass 
            self.aristogenics()
            print('Generation: {}  Global best score: {}  Current best score: {}'.format(c_iter, self.BestInd['fitness'], CBestInd['fitness']))
            # self.update_fitness_ind(self.BestInd)
            self.output_best_result(c_iter) 
        
        pass
        return self.BestInd.copy()
    pass 
    
    
    def update_fitness(self): 
        for ind in range(0, len(self.Pop['inds'])):
            while(self.Pop['inds'][ind]['modify'] == True ):
                self.Pop['inds'][ind]['fitness'] = self.fitness(self.Pop['inds'][ind]['Genes'],
                                                                self.ModelBuilder,
                                                                self.Data_handler, 
                                                                self.TrainingBasicGenerator, 
                                                                self.input_dim, 
                                                                self.input_channel_dim, 
                                                                self.output_dim, 
                                                                self.batch_size
                                                                )
                
                # if the chromosome provide broken model, generate a new individial to instead
                if self.Pop['inds'][ind]['fitness'] is np.NAN:
                    self.Pop['inds'][ind] = self.gen_ind(self.GeneNo).copy()
                else:
                    self.Pop['inds'][ind]['modify'] = False
                    self.fitness_cnt += 1
                pass 
                
            pass 
            # print('individial:{}'.format(self.Pop['inds'][ind]['fitness']))
        pass 
    pass
    
    def update_fitness_ind(self, ind):
        ind['modify'] = True
        while(ind['modify'] == True ):
            ind['fitness'] = self.fitness(ind['Genes'],
                                          self.ModelBuilder,
                                          self.Data_handler, 
                                          self.TrainingBasicGenerator, 
                                          self.input_dim, 
                                          self.input_channel_dim, 
                                          self.output_dim, 
                                          self.batch_size
                                          )
            
            # if the chromosome provide broken model, generate a new individial to instead
            if ind['fitness'] is np.NAN:
                ind = self.gen_ind(self.GeneNo).copy()
            else:
                ind['modify'] = False
                self.fitness_cnt += 1
            pass 
            
        pass 
        # print('individial:{}'.format(self.Pop['inds'][ind]['fitness']))
    pass
    
    def gen_ind(self, GeneNo):
        gen = True
        while(gen == True):
            genes = np.array( [ i for i in np.random.random(GeneNo)] )
            param = self.structure_decode(genes, #x, 
                                     7, # Max_kernel, 
                                     6, #MaxCLNo, 
                                     64, #MaxCLNodeNo, 
                                     3, #MaxFCLNO,
                                     1, #input_dim
                                     1169 # output_dim 
                                     )
            gen = param['BROKEN']
        pass
        ind = {
               'Genes' : genes,
               'fitness' : np.NAN ,
               'modify' : True
              }
        return ind 
    pass
    
    def output_best_result(self, iter): 
        param = structure_decode(self.BestInd['Genes'], #x, 
                                 7, # Max_kernel, 
                                 6, #MaxCLNo, 
                                 64, #MaxCLNodeNo, 
                                 3, #MaxFCLNO,
                                 1, #input_dim
                                 1169 # output_dim 
                                 )
        param_writer = open('./FFANEnet_log/FFANEnet_iter_{}.param'.format(iter),'w')
        param_writer.write('loss estimate:{}\n'.format(self.BestInd['fitness']))
        param_writer.write('param:\n')
        param_writer.write(str(param))
        param_writer.close()
    pass
pass


def fitness(FFANE_chromosome, 
            AACNN, 
            Data_handler, 
            TrainingBasicGenerator, 
            input_dim, 
            input_channel_dim, 
            output_dim, 
            batch_size):
            
    Data_handler.epoch = 0
    param = structure_decode(FFANE_chromosome, #x, 
                             7, # Max_kernel, 
                             6, #MaxCLNo, 
                             64, #MaxCLNodeNo, 
                             3, #MaxFCLNO,
                             1, #input_dim
                             1169 # output_dim 
                             )
    if param['BROKEN'] == True:
        # print('model broken')
        return np.NAN
    pass 
    
    score = AACNN.TDCNN_run(
                            # x, # x = tf.placeholder(tf.float32, [batch_no, input_dim[0]*input_dim[1]]) # data entry point
                            # y,
                            TrainingBasicGenerator = TrainingBasicGenerator,
                            TrainingDataHandler = Data_handler,
                            
                            struc_param = param, 
                            input_dim = input_dim, 
                            input_channel_dim = 1, 
                            output_dim = output_dim,
                            batch_size = batch_size,
                            
                            learning_rate = param['learning_rate'],
                            training_iters = 10000000,
                            display_step = 10,
                            pos_panalty = 25.78, 
                            # pos_panalty = 9,
                            PQAlpha = 0.5,
                            stop_epoch = 30
                            # stop_epoch = 1
                            )
    # print('{} '.format(score))
    return score
pass


def main():
    
    ## waiting for user's input
    print('Please input the training PSSM400:')
    TRAINING_FILE_NAME = input() 
    print('Please input the corresponding labels:')
    TRAINING_LABELS = input()
    print('Training PSSM400:{}  labels:{}'.format(TRAINING_FILE_NAME, TRAINING_LABELS))

    ## turn off the Tensorflow signal when running cuda
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # or any {'0', '1', '2'}
    
    ## read data and make the basic dataset generator
    Data_handler = FFANEnet.Data_handler()
    Tr_feature_container = Data_handler.read_dataset(TRAINING_FILE_NAME)
    Tr_lable_container   = Data_handler.read_dataset(TRAINING_LABELS)
    TrainingBasicGenerator = Data_handler.training_data_generator(Tr_feature_container, Tr_lable_container)

    ## set the entry points for construct_2dcnn
    input_dim = [20,20]
    output_dim = 1169
    batch_size = 100
    AACNN = FFANEnet.AutoArc2DCNN(SaveModel = False)
    
    ## runing the optimization of the architecture parameters
    
    # create the FFANE optimizer
    FFANEoptimizer = FFANE4FFANEnet(
                                    # the optimization parameters
                                    GeneNo = 1936, # hand calcualting this number => MaxKernel=7, MaxCLLayer=6, MaxCNodes=64, Max_FCLayer=3
                                    fitness = fitness,
                                    # Iteration = 10,
                                    Iteration = 100,
                                    # PopSize = 10, 
                                    PopSize = 40,
                                    
                                    # the CNN parameters
                                    ModelBuilder = AACNN,
                                    Data_handler = Data_handler, 
                                    TrainingBasicGenerator = TrainingBasicGenerator, 
                                    input_dim = input_dim, 
                                    input_channel_dim = 1, 
                                    output_dim = output_dim, 
                                    batch_size = batch_size,
                                    structure_decode = structure_decode
                                    
                                    )
    BestSolution = FFANEoptimizer.run()
    print('FFANEnet finished. \n The loss is {}'.format(BestSolution['fitness']))
    
    param = structure_decode(BestSolution['Genes'], #x, 
                             7, # Max_kernel, 
                             6, #MaxCLNo, 
                             64, #MaxCLNodeNo, 
                             3, #MaxFCLNO,
                             1, #input_dim
                             1169 # output_dim 
                             )
    param_writer = open('FFANEnet_best.param','w')
    param_writer.write('loss estimate:{}\n'.format(BestSolution['fitness']))
    param_writer.write('param:\n')
    param_writer.write(str(param))
    param_writer.close()
    
pass
    


def structure_decode(x, 
                     Max_kernel, 
                     MaxCLNo, 
                     MaxCLNodeNo, 
                     MaxFCLNo, 
                     input_dim,
                     output_dim):
    
    # initialize the parameters
    param = {'Active_CL':0,
             'Activated_CL_nodes':[],
             'kernel':[],
             'CL_lambda':[],
             'CL_alpha' :[],
             'CL_initial':[],
             
             'Active_FC':0,
             'FC_lambda':[],
             'FC_alpha':[],
             'FC_node':[],
             'FC_initial':[],
             
             'learning_rate': 1E-3, # 1E-n
             
             'BROKEN' : False
             
            }
    c_x = 0
    
##########
## parsing rule : ( activative_node, kernel_size, lambda, alpha, kernel_initial ) * MaxCLNo + => convoluational layers
##                ( activate_layer, node_no, lambda, alpha, node_initial) * MaxFCLNo ) + => fully connect layer
##                learning_rate
##                                                                                              markliou 20171215
##########
    
    # encoding the convolutional layer parameters
    ccl = 0
    check_edge = False
    for cl in range(0, MaxCLNo):
        LayerAlreadyActivation = False
        ActivationNodeNo = 0
        for i in range(0, MaxCLNodeNo):
            if x[c_x] >= np.random.random(): # if node activate
            
                # if the node is activate
                c_x += 1
                
                # if node "activate", but lambda and alpha are zeros, this also means "inactivation"
                if (x[c_x+1] == 0) and (x[c_x+2] == 0): 
                    c_x += 4 
                    continue 
                pass
                
                ActivationNodeNo += 1
                
                # check if the layer is activate, if not, appending a layer
                if LayerAlreadyActivation == False:
                    for i in ['kernel', 'CL_lambda', 'CL_alpha', 'CL_initial']:
                        param[i].append([])
                    pass
                    LayerAlreadyActivation = True
                pass
                
                # the kernel size => 0 ~ (kernel-1/2)
                Thresholds = np.sort(np.random.random([ np.int( ((Max_kernel-1)/2) ) ] ) )
                param['kernel'][ccl].append( np.searchsorted(Thresholds, x[c_x]) * 2 + 1)
                c_x += 1
                if not ((x[c_x-1]==0) or (x[c_x-1]==1)):
                    check_edge = True
                pass
                
                # the lambda => 0~2
                param['CL_lambda'][ccl].append(x[c_x] * 2)
                c_x += 1
                if not ((x[c_x-1]==0) or (x[c_x-1]==1)):
                    check_edge = True
                pass
                
                # the alpha => 0~2
                param['CL_alpha'][ccl].append(x[c_x] * 2)
                c_x += 1
                if not ((x[c_x-1]==0) or (x[c_x-1]==1)):
                    check_edge = True
                pass
                
                # the initial
                param['CL_initial'][ccl].append(x[c_x])
                c_x += 1
                if not ((x[c_x-1]==0) or (x[c_x-1]==1)):
                    check_edge = True
                pass
                        
            
            else: # if node is inactivate
                c_x += 5 # 5 parameters as a unit for a node, 1 layer have MaxCLNodeNo
            pass 
       
        if not ActivationNodeNo == 0:
            param['Active_CL'] += 1
            ccl += 1
            param['Activated_CL_nodes'].append(ActivationNodeNo)
        pass
    pass
    
    # encoding the fully connected layer parameters
    for fc in range(0, MaxFCLNo):
        if x[c_x] >= np.random.random(): # if layer activate
            
            c_x += 1
            
            # if layer activation is "activate", but lambda and alpha are zeros, this also means "inactivation"
            if x[c_x+1] == 0: # labmda is 0, means no acitvating
                c_x += 4
                continue 
            pass
            
            # if the layer is activate
            # # 'Active_FC':0,
            # # 'FC_node':[],
            # # 'FC_lambda':[],
            # # 'FC_alpha':[],
            # # 'FC_initial':[]
            
            param['Active_FC'] += 1
            
            # the fully connected layer node number => 1~2 times of output
            param['FC_node'].append( int( output_dim * (x[c_x] + 1) * 2) )
            # print('{} '.format(int( output_dim * (x[c_x] + 1) * 2)))
            c_x += 1
            if not ((x[c_x-1]==0) or (x[c_x-1]==1)):
                check_edge = True
            pass
            
            # the lambda => 0~2
            param['FC_lambda'].append(x[c_x] * 2)
            c_x += 1
            if not ((x[c_x-1]==1)):
                check_edge = True
            pass
            
            # the alpha => 0~2
            param['FC_alpha'].append(x[c_x] * 2)
            c_x += 1
            if not ((x[c_x-1]==0) or (x[c_x-1]==1)):
                check_edge = True
            pass
            
            # the initial
            param['FC_initial'].append(x[c_x])
            c_x += 1
            if not ((x[c_x-1]==0) or (x[c_x-1]==1)):
                check_edge = True
            pass

            
        else: # if layer is inactivate
            c_x += 5
            continue
        pass 
    
    pass 
    
    # assign learning rate
    param['learning_rate'] = np.power(0.1, (np.random.random()*4+12))
    c_x += 1
    
    # check the broken individual
    if ((param['Active_CL'] == 0) or ( param['Active_FC'] == 0 ) or (check_edge == False)):
        param['BROKEN'] = True
    pass
    
    
    return param
pass

if __name__ == '__main__':
    main()
pass
